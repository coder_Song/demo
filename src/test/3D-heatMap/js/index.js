var renderer, scene, camera, cube, controls, directionalLight;

var ww = window.innerWidth,
    wh = window.innerHeight
    , angle = 180;
var noise = new SimplexNoise(Math.random), value;

function init() {
    //准备webgl渲染器,使用canvas
    renderer = new THREE.WebGLRenderer({
        canvas: document.querySelector('canvas'),   //获取canvas标签
        antialias: true                            //是否开启抗锯齿
    });
    //设置画布大小及设置canvas背景色
    renderer.setSize(ww, wh);
    renderer.setClearColor(0xffffff);
    //创建场景
    scene = new THREE.Scene();
    //创建投影相机
    camera = new THREE.PerspectiveCamera(50, ww / wh, 0.1, 10000);
    //指定相机位置
    camera.position.set(180, 180, 180);
    //让相机始终对准物体中心
    camera.lookAt(new THREE.Vector3(0, 0, 0));
    //添加相机
    scene.add(camera);
    //添加鼠标控制相机视角
    // controls = new THREE.OrbitControls(camera);
    //添加灯光:设置灯光位置,相机视角,添加到场景中.
    directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
    directionalLight.position.set(350, 0, 0);
    directionalLight.lookAt(new THREE.Vector3(0, 0, 0));
    scene.add(directionalLight);

    directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
    directionalLight.position.set(0, 0, 350);
    directionalLight.lookAt(new THREE.Vector3(0, 0, 0));
    scene.add(directionalLight);

    directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
    directionalLight.position.set(0, 350, 0);
    directionalLight.lookAt(new THREE.Vector3(0, 0, 0));
    scene.add(directionalLight);
    createCubes();
    plane();
    render();
}

function switchColor(value, flag) {
    if (flag !== null && typeof flag !== "undefined") {
        if (value > 0 && value <= 0.4) {
            return 'rgb(24,53,154)';
        } else if (value > 0.4 && value <= 0.8) {
            return 'rgb(0,74,189)';
        } else if (value > 0.8 && value <= 1.2) {
            return 'rgb(0,118,253)';
        } else if (value > 1.2 && value <= 1.6) {
            return 'rgb(0,242,254)';
        } else if (value > 1.6 && value <= 2) {
            return 'rgb(134,255,71)';
        } else if (value > 2 && value <= 2.4) {
            return 'rgb(255,255,0)';
        } else if (value > 2.4 && value <= 2.8) {
            return 'rgb(253,207,0)';
        } else if (value > 2.8 && value <= 4) {
            return 'rgb(235,32,37)';
        }
    } else {
        if (value > 0 && value <= 0.4) {
            return new THREE.MeshLambertMaterial({color: 'rgb(24,53,154)'});
        } else if (value > 0.4 && value <= 0.8) {
            return new THREE.MeshLambertMaterial({color: 'rgb(0,74,189)'});
        } else if (value > 0.8 && value <= 1.2) {
            return new THREE.MeshLambertMaterial({color: 'rgb(0,118,253)'});
        } else if (value > 1.2 && value <= 1.6) {
            return new THREE.MeshLambertMaterial({color: 'rgb(0,242,254)'});
        } else if (value > 1.6 && value <= 2) {
            return new THREE.MeshLambertMaterial({color: 'rgb(134,255,71)'});
        } else if (value > 2 && value <= 2.4) {
            return new THREE.MeshLambertMaterial({color: 'rgb(255,255,0)'});
        } else if (value > 2.4 && value <= 2.8) {
            return new THREE.MeshLambertMaterial({color: 'rgb(253,207,0)'});
        } else if (value > 2.8 && value <= 4) {
            return new THREE.MeshLambertMaterial({color: 'rgb(235,32,37)'});
        }
    }

}

var cubes = new THREE.Object3D()
    , cubes1 = new THREE.Object3D()
    , cubes2 = new THREE.Object3D()
    , cubes3 = new THREE.Object3D();

function createCubes() {
    //创建圆柱体
    let geometry = new THREE.CylinderGeometry(1, 1, 20, 20, 20);
    /* var color = new THREE.MeshLambertMaterial({color: 'rgb(24,53,154)'})
     , color1 = new THREE.MeshLambertMaterial({color: 'rgb(0,74,189)'})
     , color2 = new THREE.MeshLambertMaterial({color: 'rgb(0,118,253)'})
     , color3 = new THREE.MeshLambertMaterial({color: 'rgb(0,242,254)'})
     , color4 = new THREE.MeshLambertMaterial({color: 'rgb(134,255,71)'})
     , color5 = new THREE.MeshLambertMaterial({color: 'rgb(255,255,0)'})
     , color6 = new THREE.MeshLambertMaterial({color: 'rgb(253,207,0)'})
     , color7 = new THREE.MeshLambertMaterial({color: 'rgb(235,32,37)'});*/
    //循环添加多个圆柱体
    for (var i = -22; i < -14; i++) {
        for (var j = -24; j < 24; j++) {
            value = noise.noise2D(i / 14, j / 14) + 2;
            //添加材质及样式
            cube = new THREE.Mesh(geometry, switchColor(value));
            //位置
            cube.position.x = 3 * i;
            cube.position.z = 3 * j;
            //高度
            cube.scale.y = 0.1;
            cube.index = Math.sqrt((cube.position.x) * (cube.position.x) + (cube.position.x) * (cube.position.z)) * 0.1;
            //渲染圆柱体拔高动画
            TweenMax.to(cube.scale, 1.6, {
                y: value,                   //高度
                repeat: 0,                 //循环次数,-1为无限循环
                repeatDelay: 3,             //延迟播放
                yoyo: true,                 //设置true表示有动画过程
                delay: cube.index * 0.18,   //圆柱体动画播放间隔
                ease: Power2.easeInOut,     //动画样式
            });
            cubes.add(cube);                //添加圆柱体

        }
    }

    for (var i = -12; i < 0; i++) {
        for (var j = -24; j < 24; j++) {
            value = noise.noise2D(i / 14, j / 14) + 2;
            cube = new THREE.Mesh(geometry, switchColor(value));
            cube.position.x = 3 * i;
            cube.position.z = 3 * j;
            cube.scale.y = 0.1;
            cube.index = Math.sqrt((cube.position.x) * (cube.position.x) + (cube.position.x) * (cube.position.z)) * 0.1;

            TweenMax.to(cube.scale, 1.6, {
                y: value,
                repeat: 0,
                repeatDelay: 3,
                yoyo: true,
                delay: cube.index * 0.18,
                ease: Power2.easeInOut,
            });
            cubes1.add(cube);

        }
    }

    for (var i = 2; i < 12; i++) {
        for (var j = -24; j < 24; j++) {
            value = noise.noise2D(i / 14, j / 14) + 2;
            cube = new THREE.Mesh(geometry, switchColor(value));
            cube.position.x = 3 * i;
            cube.scale.y = 0.1;
            cube.position.z = 3 * j;
            cube.index = Math.sqrt((cube.position.x) * (cube.position.x) + (cube.position.x) * (cube.position.z)) * 0.1;

            TweenMax.to(cube.scale, 1.6, {
                y: value,
                repeat: 0,
                repeatDelay: 3,
                yoyo: true,
                delay: cube.index * 0.18,
                ease: Power2.easeInOut,
            });
            cubes2.add(cube);

        }
    }

    for (var i = 14; i < 24; i++) {
        for (var j = -24; j < 24; j++) {
            value = noise.noise2D(i / 14, j / 14) + 2;
            cube = new THREE.Mesh(geometry, switchColor(value));
            cube.position.x = 3 * i;
            cube.scale.y = 0.1;
            cube.position.z = 3 * j;
            cube.index = Math.sqrt((cube.position.x) * (cube.position.x) + (cube.position.x) * (cube.position.z)) * 0.1;

            TweenMax.to(cube.scale, 1.6, {
                y: value,
                repeat: 0,
                repeatDelay: 3,
                yoyo: true,
                delay: cube.index * 0.18,
                ease: Power2.easeInOut,
            });

            cubes3.add(cube);

        }
    }

    scene.add(cubes);
    scene.add(cubes1);
    scene.add(cubes2);
    scene.add(cubes3);
}

setInterval(function () {
    for (var i = 0; i < cubes.children.length; i++) {
        value = noise.noise2D(i / 14, i / 24) + 2 - Math.random();
        TweenMax.to(cubes.children[i].scale, 0.8, {
            y: value,
            repeat: 0,
            repeatDelay: 0,
            yoyo: true,
            delay: cubes.children[i].index * 0.18,
            ease: Power0.easeNone,
        });

        cubes.children[i].material.color.setStyle(switchColor(value, true));
    }

    setTimeout(function () {
        for (var i = 0; i < cubes1.children.length; i++) {
            value = noise.noise2D(i / 14, i / 24) + 2 - Math.random();
            TweenMax.to(cubes1.children[i].scale, 0.8, {
                y: value,
                repeat: 0,
                repeatDelay: 0.9,
                yoyo: true,
                delay: cubes1.children[i].index * 0.18,
                ease: Power0.easeNone,
            });

            cubes1.children[i].material.color.setStyle(switchColor(value, true));
        }
    }, 1800);

    setTimeout(function () {
        for (var i = 0; i < cubes2.children.length; i++) {
            value = noise.noise2D(i / 14, i / 24) + 2 - Math.random();
            TweenMax.to(cubes2.children[i].scale, 0.8, {
                y: value,
                repeat: 0,
                repeatDelay: 1.8,
                yoyo: true,
                delay: cubes2.children[i].index * 0.18,
                ease: Power0.easeNone,
            });

            cubes2.children[i].material.color.setStyle(switchColor(value, true));

        }
    }, 3000);

    setTimeout(function () {
        for (var i = 0; i < cubes3.children.length; i++) {
            value = noise.noise2D(i / 14, i / 24) + 2 - Math.random();
            TweenMax.to(cubes3.children[i].scale, 0.8, {
                y: value,
                repeat: 0,
                repeatDelay: 2.7,
                yoyo: true,
                delay: cubes3.children[i].index * 0.18,
                ease: Power0.easeNone,
            });
            cubes3.children[i].material.color.setStyle(switchColor(value, true));
        }

    }, 4000);
}, 10 * 1000);


//创建一个平面
function plane() {
    let planeGeo = new THREE.PlaneGeometry(180, 180, 10, 10);//创建平面
    let planeMat = new THREE.MeshLambertMaterial({  //创建材料
        color: '#2c1e3c',
        wireframe: false
    });
    planeMesh = new THREE.Mesh(planeGeo, planeMat);//创建网格模型
    planeMesh.position.set(0, 0, 0);//设置平面的坐标
    planeMesh.rotation.x = -0.5 * Math.PI;//将平面绕X轴逆时针旋转90度
    scene.add(planeMesh);//将平面添加到场景中

    // let edges = new THREE.EdgesHelper(planeMesh, 0x1535f7);//设置边框，可以旋转

    let edges = new THREE.EdgesGeometry(planeGeo);

    let line = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({color: 0x1535f7}));
    line.rotation.x = -0.5 * Math.PI;
    scene.add(line);

}

var render = function () {
    requestAnimationFrame(render);

    angle += 0.003;
    // 重新设置相机位置，相机在XOY平面绕着坐标原点旋转运动
    camera.position.x = 200 * Math.sin(angle);
    camera.position.z = 200 * Math.cos(angle);
    // 相机位置改变后，注意执行.looAt()方法重新计算视图矩阵旋转部分
    // 如果不执行.looAt()方法，相当于相机镜头方向保持在首次执行`.lookAt()`的时候
    camera.lookAt(new THREE.Vector3(0, 0, 0));
    renderer.render(scene, camera);
};

init();